package com.example.salaryapi.entity;

import com.example.salaryapi.enums.Department;
import com.example.salaryapi.enums.Duty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer employeeNumber;
    @Column(nullable = false ,length = 20)
    private String name;
    @Column(nullable = false)
    private Department affiliatedDepartment;
    @Column(nullable = false)
    private Duty dutyClass;
    @Column(nullable = false)
    private Double beforeTax;
    @Column(nullable = false)
    private Double noTax;
}
