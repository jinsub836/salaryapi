package com.example.salaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Department {

    PERSONNEL("인사부"),
    GENERAL_AFFAIRS("총무부서")
    ,FACILITIES("시설부서")
    ,QUALITY("품질부서")
    ,PRODUCTION("생산부서")
    ,SALES ("영업부서")
    ,DEVELOPMENT("개발부서")
    ,OWNER("사장님");

    private final String affiliatedDepartment;

}
