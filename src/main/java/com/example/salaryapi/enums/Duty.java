package com.example.salaryapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Duty {
    CEO("사장")
    ,VICE_PRESIDENT("부사장")
    ,EXECUTIVE_DIRECTOR("전무")
    ,GENERAL("총괄이사")
    ,HEAD_OF_DEPARTMENT("부장")
    ,DEPUTY_DIRECTOR("차장")
    ,SECTION_CHIEF("과장")
    ,DEPUTY_SECTION("대리")
    ,MANAGER("주임")
    ,EMPLOYEE("사원");

    private final String dutyClass;
}
